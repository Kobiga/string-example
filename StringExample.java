
public class StringExample {


	
	public static void main(String args[]) {
		String campus="BCAS";
		String location="jaffna";
		
		//System.out.println(campus.toLowerCase());//40// output:bcas
		//System.out.println(location.toUpperCase());//43// output:JAFFNA
		//System.out.print(location.concat(campus));//5// output:jaffnaBCAS
		//System.out.print(campus.charAt(3));//1// output:S

		
		String msg= "welcome    ";
	    String to ="to";
	    String  text1="This is really not immutable";
	    String  text2="This is really not immutable";
	    String  text3="This is really not immutable";
	    String  text4="THIS IS REALLY NOT IMMUTABLE";
	    String  text5="welcome to BCAS ";
	    String  text6="welcome to BCAS ";
	    String  subtext6="hi";
	    
	    
		boolean retval;
		
		//System.out.println(text1.endsWith("immutable"));//9// output:true
		//System.out.println(text1.endsWith("immu"));//9// output:false
	//	System.out.println(text1.endsWith("immutable!!"));//9// output: false
		//System.out.println(msg);//output:welcome    

		//System.out.println(msg.trim());//45//output:welcome
		//System.out.println(msg.concat(to).concat(campus));//5//output:welcome    toBCAS
	//	System.out.println(msg.trim().concat(to).concat(campus));//5// output:welcometoBCAS
		//System.out.println(msg.toString());//output:welcome
	//	System.out.println(msg.toCharArray());//39//output:welcome
	
	//System.out.println(text1.equals(text2));//11//output:true
		//System.out.println(text1.equals(text4));//11//output:false
		//System.out.println(text1.equalsIgnoreCase(text4));//11//output:true
		//System.out.println(text1.equalsIgnoreCase(text3));//11//output:false
		//System.out.println(text5.indexOf('o'));//16//output:4
		//System.out.println(text5.indexOf('w'));//16//output:0
		
		//System.out.println(text5.indexOf('o',5));//17//output:9
		//System.out.println(text5.indexOf('e',1));//17//output:1
		//System.out.println(text6.indexOf(subtext6));//18//output:-1
	//System.out.println(text6.indexOf(subtext6,5));//19//output:-1
		//System.out.println(text6.lastIndexOf('e'));//21//output:6
		//System.out.println(text6.lastIndexOf('e',2));//22//output:1
		//System.out.println(text6.lastIndexOf(subtext6));//23//output:-1
		//System.out.println( text6.indexOf(subtext6,9));//24//output:-1
		//System.out.println( to.length());//25//output:2
	//	System.out.println( to.replace('t','o'));//29//output:oo
		//System.out.println( text6.substring(9));//37//output:o BCAS
		System.out.println( text6.substring(9,12));//38//output:o B
		
		




		
		
		
}
}


